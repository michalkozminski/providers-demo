var fs = require('fs');
var pg = require('pg');
var client = new pg.Client({ connectionString: process.env.DATABASE_URL })

var copyFrom = require('pg-copy-streams').from;

client.connect();
var stream = client.query(copyFrom("COPY providers FROM STDIN CSV"));
var fileStream = fs.createReadStream('migrationData.csv')
fileStream.on('error', (err) => {
	console.error(arguments);
	process.exit(1);
});
fileStream.pipe(stream).on('finish', (err, res) => {
	console.error(res);
	console.error(err);
	process.exit(0);
})
stream.on('end', (err, status) => {
	console.log(err);
	console.log(status);

	process.exit(1);
});

