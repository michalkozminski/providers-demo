import test from 'ava';
import request from 'supertest';
import { Client } from 'pg';
import app from './app';


const example = [{
  'Provider Name': 'SOUTHEAST ALABAMA MEDICAL CENTER', 'Provider Street Address': '1108 ROSS CLARK CIRCLE', 'Provider City': 'DOTHAN', 'Provider State': 'AL', 'Provider Zip Code': '36301', 'Hospital Referral Region Description': 'AL - Dothan', 'Total Discharges': '91', 'Average Covered Charges': '32963.07', 'Average Total Payments': '5777.24', 'Average Medicare Payments': '4763.73',
}, {
  'Provider Name': 'MARSHALL MEDICAL CENTER SOUTH', 'Provider Street Address': '2505 U S HIGHWAY 431 NORTH', 'Provider City': 'BOAZ', 'Provider State': 'AL', 'Provider Zip Code': '35957', 'Hospital Referral Region Description': 'AL - Birmingham', 'Total Discharges': '14', 'Average Covered Charges': '15131.85', 'Average Total Payments': '5787.57', 'Average Medicare Payments': '4976.71',
}, {
  'Provider Name': 'ELIZA COFFEE MEMORIAL HOSPITAL', 'Provider Street Address': '205 MARENGO STREET', 'Provider City': 'FLORENCE', 'Provider State': 'AL', 'Provider Zip Code': '35631', 'Hospital Referral Region Description': 'AL - Birmingham', 'Total Discharges': '24', 'Average Covered Charges': '37560.37', 'Average Total Payments': '5434.95', 'Average Medicare Payments': '4453.79',
}];

test.before(async () => {
  const client = new Client({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DATABASE,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
  });

  await client.connect();
  await client.query('DELETE FROM providers');
  await client.query('INSERT INTO "public"."providers" ("drg_definition", "provider_id", "provider_name", "provider_street_address", "provider_city", "provider_state", "provider_zip_code", "hospital_referral_region_description", "total_discharges", "average_covered_charges", "average_total_payments", "average_medicare_payments") VALUES (\'039 - EXTRACRANIAL PROCEDURES W/O CC/MCC\', 10001, \'SOUTHEAST ALABAMA MEDICAL CENTER\', \'1108 ROSS CLARK CIRCLE\', \'DOTHAN\', \'AL\', \'36301\', \'AL - Dothan\', 91, 32963.07, 5777.24, 4763.73)');
  await client.query('INSERT INTO "public"."providers" ("drg_definition", "provider_id", "provider_name", "provider_street_address", "provider_city", "provider_state", "provider_zip_code", "hospital_referral_region_description", "total_discharges", "average_covered_charges", "average_total_payments", "average_medicare_payments") VALUES (\'039 - EXTRACRANIAL PROCEDURES W/O CC/MCC\', 10005, \'MARSHALL MEDICAL CENTER SOUTH\', \'2505 U S HIGHWAY 431 NORTH\', \'BOAZ\', \'AL\', \'35957\', \'AL - Birmingham\', 14, 15131.85, 5787.57, 4976.71)');
  await client.query('INSERT INTO "public"."providers" ("drg_definition", "provider_id", "provider_name", "provider_street_address", "provider_city", "provider_state", "provider_zip_code", "hospital_referral_region_description", "total_discharges", "average_covered_charges", "average_total_payments", "average_medicare_payments") VALUES (\'039 - EXTRACRANIAL PROCEDURES W/O CC/MCC\', 10006, \'ELIZA COFFEE MEMORIAL HOSPITAL\', \'205 MARENGO STREET\', \'FLORENCE\', \'AL\', \'35631\', \'AL - Birmingham\', 24, 37560.37, 5434.95, 4453.79)');
});


test('it returns all values', t => request(app)
  .get('/providers')
  .then((res) => {
    t.deepEqual(res.body, example);
  }));

test('it returns maching results to passed params', t => request(app)
  .get('/providers?max_discharges=91&min_discharges=23')
  .then((res) => {
    t.deepEqual(res.body, [example[2], example[0]]);
  }));
