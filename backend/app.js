import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import providersQuery from './providers/providersQuery';

const app = express();

app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(cors());

app.get('/providers', async (req, res) => {
  try {
    const providers = await providersQuery(req.query);
    res.json(providers);
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
});

app.get('/health', (req, res) => {
  res.json({ status: 'UP' });
});

export default app;
