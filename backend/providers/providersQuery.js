/* eslint camelcase: 0 */
import { Pool } from 'pg';

import queryBuilder from './queryBuilder';

const client = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DATABASE,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
});

client.connect();

function providerRowsToDtos(rows) {
  return rows.map(row => ({
    'Provider Name': row.provider_name,
    'Provider Street Address': row.provider_street_address,
    'Provider City': row.provider_city,
    'Provider State': row.provider_state,
    'Provider Zip Code': row.provider_zip_code,
    'Hospital Referral Region Description': row.hospital_referral_region_description,
    'Total Discharges': row.total_discharges,
    'Average Covered Charges': row.average_covered_charges,
    'Average Total Payments': row.average_total_payments,
    'Average Medicare Payments': row.average_medicare_payments,
  }));
}

async function providersQuery(params) {
  const res = await client.query(queryBuilder(params));
  return providerRowsToDtos(res.rows);
}

export default providersQuery;
/* eslint camelcase: 2 */
