import Knex from 'knex';

function buildWhereCause(query) {
  const {
    max_discharges,
    min_discharges,
    max_average_covered_charges,
    min_average_covered_charges,
    max_average_medicare_payments,
    min_average_medicare_payments,
    state,
    fields,
    limit,
  } = query;

  const selectedColumns = fields || ['*'];
  let baseQuery = Knex({ client: 'pg' })('providers').select(...selectedColumns);

  if (max_discharges) { baseQuery = baseQuery.where('total_discharges', '<=', max_discharges); }
  if (min_discharges) { baseQuery = baseQuery.where('total_discharges', '>=', min_discharges); }
  if (max_average_covered_charges) { baseQuery = baseQuery.where('average_covered_charges', '<=', max_average_covered_charges); }
  if (min_average_covered_charges) { baseQuery = baseQuery.where('average_covered_charges ', '>=', min_average_covered_charges); }
  if (max_average_medicare_payments) { baseQuery = baseQuery.where('average_medicare_payments', '<=', max_average_medicare_payments); }
  if (min_average_medicare_payments) { baseQuery = baseQuery.where('average_medicare_payments', '>=', min_average_medicare_payments); }
  if (state) { baseQuery = baseQuery.where('provider_state', state); }
  if (limit) { baseQuery = baseQuery.limit(limit); }

  return baseQuery.toString();
}

export default buildWhereCause;
