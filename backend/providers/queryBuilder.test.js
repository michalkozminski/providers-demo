import test from 'ava';
import queryBuilder from './queryBuilder';

test('empty object willl render empty where ', (t) => {
  t.is(queryBuilder({}), 'select * from "providers"');
});

test('only one parameter will render single value', (t) => {
  t.is(queryBuilder({ max_discharges: 5 }), 'select * from "providers" where "total_discharges" <= 5');
});

test('use two parameters', (t) => {
  t.is(queryBuilder({ max_discharges: 5, min_average_medicare_payments: 2 }), 'select * from "providers" where "total_discharges" <= 5 and "average_medicare_payments" >= 2');
});

test('select certain columns', (t) => {
  t.is(queryBuilder({ fields: ['provider_city', 'provider_name'] }), 'select "provider_city", "provider_name" from "providers"');
});

test('apply limit to query', (t) => {
  t.is(queryBuilder({ limit: 200 }), 'select * from "providers" limit 200');
});
