exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('providers', {
    drg_definition: { type: 'varchar(200)', notNull: true },
    provider_id: { type: 'int', notNull: true },
    provider_name: { type: 'varchar(200)', notNull: true },
    provider_street_address: { type: 'varchar(200)', notNull: true },
    provider_city: { type: 'varchar(200)', notNull: true },
    provider_state: { type: 'varchar(2)', notNull: true },
    provider_zip_code: { type: 'varchar(5)', notNull: true },
    hospital_referral_region_description: { type: 'varchar(200)', notNull: true },
    total_discharges: { type: 'decimal', notNull: true },
    average_covered_charges: { type: 'decimal', notNull: true },
    average_total_payments: { type: 'decimal', notNull: true },
    average_medicare_payments: { type: 'decimal', notNull: true },

  });
  pgm.createIndex('providers', 'total_discharges');
  pgm.createIndex('providers', 'average_covered_charges');
  pgm.createIndex('providers', 'average_medicare_payments');
  pgm.createIndex('providers', 'provider_state');
};

exports.down = (pgm) => {
  pgm.dropTable('providers');
};
