import querystring from 'query-string';
import { fetch } from 'whatwg-fetch';

export const FETCH_PROVIDERS = 'FETCH_PROVIDERS';
export const FETCH_PROVIDERS_SUCCESS = 'FETCH_PROVIDERS_SUCCESS';
export const FETCH_PROVIDERS_FAILURE = 'FETCH_PROVIDERS_FAILURE';

export const SET_PROVIDERS_FILTER = 'SET_PROVIDERS_FILTER';
export const SET_PROVIDERS_FIELDS = 'SET_PROVIDERS_FIELDS';
export const SET_PROVIDERS_LIMIT = 'SET_PROVIDERS_LIMIT';

const PROVIDERS_URL = process.env.PROVIDERS_URL;

export function requestProviders() {
  return {
    type: FETCH_PROVIDERS,
  };
}

export function receiveProvidersSuccess(providers) {
  return {
    type: FETCH_PROVIDERS_SUCCESS,
    providers,
  };
}

export function receiveProvidersFailure(error) {
  /*eslint-disable */
  alert('Error fetching providers');
  /*eslint-enable */
  return {
    type: FETCH_PROVIDERS_FAILURE,
    error,
  };
}

export function setFilter(filter) {
  return {
    type: SET_PROVIDERS_FILTER,
    filter,
  };
}

export function setFields(field) {
  return {
    type: SET_PROVIDERS_FIELDS,
    field,
  };
}

export function setLimit(limit) {
  return {
    type: SET_PROVIDERS_LIMIT,
    limit,
  };
}

export const fetchProviders = () => (dispatch, getState) => {
  const { providers } = getState();
  const { filters, fields: fieldsObj, limit } = providers;

  const fields = Object.entries(fieldsObj)
    .map(([key, value]) => (value ? key : undefined))
    .filter(v => v);

  dispatch(requestProviders());

  return fetch(`${PROVIDERS_URL}?${querystring.stringify({ ...filters, fields, limit }, { arrayFormat: 'bracket' })}`)
    .then(res => res.json())
    .then((providersList) => {
      dispatch(receiveProvidersSuccess(providersList));
    }, err => dispatch(receiveProvidersFailure(err)));
};
