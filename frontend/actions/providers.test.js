import { fetchProviders } from './providers';

jest.mock('whatwg-fetch');
const fetchObj = require('whatwg-fetch');

describe('#fetchProviders', () => {
  const providersState = {
    filters: { state: 'AL' },
    fields: { state: true, provider_id: false },
    limit: 100,
  };
  const fakeDispatch = jest.fn(() => {});

  describe('given filters, limit, fields', () => {
    beforeAll(() => {
      fetchObj.fetch.mockImplementation(() => Promise.resolve({}));
    });

    it('should call fetch with right URL', () => {
      fetchProviders()(() => {}, () => ({ providers: providersState }));
      expect(fetchObj.fetch).toHaveBeenCalledWith('http://backend?fields[]=state&limit=100&state=AL');
    });
  });

  describe('given sucessfull fetch response', () => {
    beforeAll(() => {
      fetchObj.fetch.mockImplementation(() => Promise.resolve({ json: () => [{ id: '1' }] }));
    });

    it('should dispatch FETCH_PROVIDERS_SUCCESS', async () => {
      await fetchProviders()(fakeDispatch, () => ({ providers: providersState }));

      expect(fakeDispatch.mock.calls).toEqual([
        [{ type: 'FETCH_PROVIDERS' }],
        [{ type: 'FETCH_PROVIDERS_SUCCESS', providers: [{ id: '1' }] }],
      ]);
    });
  });

  describe('given unsucessful fetch response', () => {
    beforeAll(() => {
      fetchObj.fetch.mockImplementation(() => Promise.reject('error'));
    });

    it('should dispatch FETCH_PROVIDERS_SUCCESS', async () => {
      await fetchProviders()(fakeDispatch, () => ({ providers: providersState }));

      expect(fakeDispatch.mock.calls).toEqual([
        [{ type: 'FETCH_PROVIDERS' }],
        [{ type: 'FETCH_PROVIDERS_FAILURE', error: 'error' }],
      ]);
    });
  });
});
