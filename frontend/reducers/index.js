import { combineReducers } from 'redux';
import {
  FETCH_PROVIDERS_SUCCESS,
  SET_PROVIDERS_FIELDS,
  SET_PROVIDERS_FILTER,
  FETCH_PROVIDERS,
  SET_PROVIDERS_LIMIT,
} from '../actions/providers';

const providers = (state = {
  data: [],
  limit: 500,
  fields: {
    provider_name: true,
    provider_street_address: true,
    provider_city: true,
    provider_state: true,
    provider_zip_code: true,
    hospital_referral_region_description: true,
    total_discharges: true,
    average_covered_charges: true,
    average_total_payments: true,
    average_medicare_payments: true,
  } }, action) => {
  switch (action.type) {
    case FETCH_PROVIDERS:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_PROVIDERS_SUCCESS:
      return {
        ...state,
        data: action.providers,
        isLoading: false,
      };
    case SET_PROVIDERS_FIELDS:
      return {
        ...state,
        fields: {
          ...state.fields,
          [action.field]: !state.fields[action.field],
        },
      };
    case SET_PROVIDERS_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          ...action.filter,
        },

      };
    case SET_PROVIDERS_LIMIT:
      return {
        ...state,
        limit: action.limit,
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  providers,
});

export default rootReducer;
