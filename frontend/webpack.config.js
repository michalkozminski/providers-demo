const webpack = require('webpack');

module.exports = function (webpackConfig) {
  webpackConfig.plugins.push(new webpack.DefinePlugin({
    'process.env.PROVIDERS_URL': JSON.stringify(process.env.PROVIDERS_URL),
  }));
  webpackConfig.babel.plugins.push('transform-runtime');
  webpackConfig.babel.plugins.push('transform-runtime');
  webpackConfig.babel.plugins.push(['import', {
    libraryName: 'antd',
    style: 'css',
  }]);

  return webpackConfig;
};
