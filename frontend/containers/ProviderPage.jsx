import React from 'react';
import { connect } from 'react-redux';
import { Table, Input, Button, Form, Switch } from 'antd';
import { debounce } from 'underscore';

import {
  fetchProviders as fetchProvidersAction,
  setFilter as setFilterAction,
  setLimit as setLimitAction,
  setFields as setFieldsAction,
} from '../actions/providers';

const handleFilter = (query, confirm, filter) => () => {
  confirm();
  filter(query);
};

const ProviderPage = ({
  providersList, filter, toggleField, fields, isLoading, limit, setLimit,
}) => {
  console.dir({ providersList, filter, toggleField, fields, isLoading, limit, setLimit });
  const columns = [
    { title: 'Provider Name', dataIndex: 'Provider Name', key: 'provider_name' },
    { title: 'Provider Street Address',
      dataIndex: 'Provider Street Address',
      key: 'provider_street_address',
    },
    { title: 'Provider City', dataIndex: 'Provider City', key: 'provider_city' },
    { title: 'Provider State',
      dataIndex: 'Provider State',
      key: 'provider_state',
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
        <div className="custom-filter-dropdown">
          <Input
            placeholder="AL"
            value={selectedKeys}
            onChange={e => setSelectedKeys(e.target.value)}
          />
          <Button type="primary" onClick={handleFilter({ state: selectedKeys }, confirm, filter)}>
            Search
          </Button>
          <Button onClick={handleFilter({ state: undefined }, confirm, filter)}>Reset</Button>
        </div>
      ),
    },
    { title: 'Provider Zip Code', dataIndex: 'Provider Zip Code', key: 'provider_zip_code' },
    { title: 'Hospital Referral Region Description',
      dataIndex: 'Hospital Referral Region Description',
      key: 'hospital_referral_region_description',
    },
    { title: 'Total Discharges', dataIndex: 'Total Discharges', key: 'total_discharges' },
    { title: 'Average Covered Charges',
      dataIndex: 'Average Covered Charges',
      key: 'average_covered_charges',
      filterDropdown: ({ setSelectedKeys, selectedKeys = {}, confirm, clearFilter }) => (
        <div className="custom-filter-dropdown">
          From:
          <Input
            placeholder="0"
            value={selectedKeys.min_average_covered_charges}
            onChange={e => setSelectedKeys({
              ...selectedKeys, min_average_covered_charges: e.target.value,
            })}
          />
          To:
          <Input
            placeholder="1000"
            value={selectedKeys.max_average_covered_charges}
            onChange={e => setSelectedKeys({
              ...selectedKeys, max_average_covered_charges: e.target.value,
            })}
          />
          <Button
            type="primary"
            onClick={handleFilter(selectedKeys, confirm, filter)}
          >Search</Button>
          <Button
            onClick={handleFilter({
              min_average_covered_charges: undefined,
              max_average_covered_charges: undefined },
              clearFilter, filter)}
          >
            Reset
          </Button>
        </div>
      ),
    },
    { title: 'Average Total Payments', dataIndex: 'Average Total Payments', key: 'average_total_payments' },
    { title: 'Average Medicare Payments',
      dataIndex: 'Average Medicare Payments',
      key: 'average_medicare_payments',
      filterDropdown: ({ setSelectedKeys, selectedKeys = {}, confirm, clearFilter }) => (
        <div className="custom-filter-dropdown">
          From:
          <Input
            placeholder="0"
            value={selectedKeys.min_average_medicare_payments}
            onChange={e => setSelectedKeys({
              ...selectedKeys, min_average_medicare_payments: e.target.value,
            })}
          />
          To:
          <Input
            placeholder="1000"
            value={selectedKeys.max_average_medicare_payments}
            onChange={e => setSelectedKeys({
              ...selectedKeys, max_average_medicare_payments: e.target.value,
            })}
          />
          <Button
            type="primary"
            onClick={handleFilter(selectedKeys, confirm, filter)}
          >Search</Button>
          <Button
            onClick={handleFilter({
              max_average_medicare_payments: undefined,
              min_average_medicare_payments: undefined },
            clearFilter, filter)}
          >
            Reset
          </Button>
        </div>
      ),

    },
  ];
  const filterColumns = (col, fieldsObj) => col.filter(c => fieldsObj[c.key]);
  return (
    <div style={{ background: '#fff', padding: 0, minHeight: 280 }}>
      <Form layout="inline">
        <Form.Item label="Provider Name">
          <Switch checked={fields.provider_name} onChange={toggleField('provider_name')} />
        </Form.Item>
        <Form.Item label="Provider Street Address">
          <Switch checked={fields.provider_street_address} onChange={toggleField('provider_street_address')} />
        </Form.Item>
        <Form.Item label="Provider City">
          <Switch checked={fields.provider_city} onChange={toggleField('provider_city')} />
        </Form.Item>
        <Form.Item label="Provider State">
          <Switch checked={fields.provider_state} onChange={toggleField('provider_state')} />
        </Form.Item>
        <Form.Item label="Provider Zip Code">
          <Switch checked={fields.provider_zip_code} onChange={toggleField('provider_zip_code')} />
        </Form.Item>
        <Form.Item label="Hospital Referral Region Description">
          <Switch checked={fields.hospital_referral_region_description} onChange={toggleField('hospital_referral_region_description')} />
        </Form.Item>
        <Form.Item label="Total Discharges">
          <Switch checked={fields.total_discharges} onChange={toggleField('total_discharges')} />
        </Form.Item>
        <Form.Item label="Average Covered Charges">
          <Switch checked={fields.average_covered_charges} onChange={toggleField('average_covered_charges')} />
        </Form.Item>
        <Form.Item label="Average Total Payments">
          <Switch checked={fields.average_total_payments} onChange={toggleField('average_total_payments')} />
        </Form.Item>
        <Form.Item label="Average Medicare Payments">
          <Switch checked={fields.average_medicare_payments} onChange={toggleField('average_medicare_payments')} />
        </Form.Item>
        <Form.Item label="Limit">
          <Input value={limit} onChange={e => setLimit(e.target.value)} />
        </Form.Item>
      </Form>
      <Table
        dataSource={providersList}
        columns={filterColumns(columns, fields)}
        pagination={false}
        scroll={{ x: 2050 }}
        loading={isLoading}
        rowKey={row => JSON.stringify(row)}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  const { providers: { data, fields, isLoading, limit } } = state;
  return { providersList: data, fields, isLoading, limit };
};

const mapDispatchToProps = (dispatch) => {
  const debouncedFetch = debounce(() => dispatch(fetchProvidersAction()), 400);
  return {
    filter: (filter) => {
      dispatch(setFilterAction(filter));
      dispatch(fetchProvidersAction());
    },
    setLimit: (limit) => {
      dispatch(setLimitAction(limit));
      debouncedFetch();
    },
    toggleField: fieldName => () => {
      dispatch(setFieldsAction(fieldName));
      dispatch(fetchProvidersAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProviderPage);
