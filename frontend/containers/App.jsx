import React from 'react';
import { connect } from 'react-redux';
import { Layout } from 'antd';

import ProviderPage from './ProviderPage';
import { fetchProviders } from '../actions/providers';

const { Header, Content, Footer } = Layout;

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    dispatch(fetchProviders({}));
  }

  render() {
    return (
      <Layout className="layout">
        <Header>
          <div className="logo" />
        </Header>
        <Content style={{ padding: 0 }}>
          <ProviderPage />
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Demo App Footer 2018
        </Footer>
      </Layout>
    );
  }
}


export default connect()(App);
