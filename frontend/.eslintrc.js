module.exports = {
  "extends": "airbnb",
  "env": {
    "jest": true,
    "browser": true
  },
  "plugins": [
    "react",
    "jsx-a11y",
    "import"
  ],
  "rules": {
    "react/prop-types": "off"
  }

};
