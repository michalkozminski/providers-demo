# Demo
UI:
http://35.233.120.81/

API:

http://35.241.192.34/providers

# Assumptions
- There is API requirments of:
```
[
  {
    "Provider Name": "SOUTHEAST ALABAMA MEDICAL CENTER",
    "Provider Street Address": "1108 ROSS CLARK CIRCLE",
    "Provider City": "DOTHAN",
    "Provider State": "AL",
    "Provider Zip Code": "36301",
    "Hospital Referral Region Description": "AL - Dothan",
    "Total Discharges": 91,
    "Average Covered Charges": "$32,963.07",
    "Average Total Payments":   "$5,777.24",
    "Average Medicare Payments": "$4,763.73"
  }
]
```
- There is a chance of data growth
- The user might want to download all data at once
- User want to view all data at once

# Architecture

## API
- Assumed to have already defined API as one above
  + Probably because of streaming
- Added limit to API which helps render only part of data
- Would recoment putting Array into object so response would look like:
```
{ data: [ ... ] }
```
- This would allow adding pagination/metadata in future with non breaking API
- Pagination should be made by currsor + limit which would cause need for tech ID

## DevOps
- As backend deployment we use k8s to allow easier migration
  + Cloud oriented architecture which can be depoyed on any major cloud provider
- Use PostgreSQL as service from google cloud
  + Reduce maintaince
- Run migrations before each deployment
- Need of non breaking migrations (rolling updates)

## Testing
- Create integration/unit testing for backend using ava
  + Very tiny framework
  + Allows easy migrate to other framework when app groths
- Eslint

## Frontend
- Using pure JavaScript ES 2016
  + Since complexity of app is really low there is no need to use TypeScript
  + Compliation to ealier versions using babel for browser copability
- Use React with premade components Ant.design
  + Pretested view layer
  + Made for admin portal like apps
- Use Jest testing
  + Possible testing of react components (when custom writen)
- Use Redux
  + Alows extracting commands for testing purpose
  + Easy state managing lib
  + Avoid mutations of state
- Added default limit of 500 rows to prefech
  + To make sure that website can be easily rendered (no out of memory errors)

## Backend
- Using node JS
  + To allow single developer easily write Frontend/backend
  + Easy migration to streaming API
    * In case of data groth
- Using Express JS
  + Well known framework
- Single layer of query
  + Allows access to PG withouth using full blown ORM + Model when not needed
  + Easier migration to pg streaming if needed
- Using Docker-compose for integration testing
  + Make sure that developer can run tests on any configuration

# To improve

- Test covertage
  + Add view layer testing (not included since we use ant.desing)
  + Add integration testing backend/frontend
- Add Dev docker files
  + Allows faster oboarding of developers
- Undestanding domain
  + There are no model boundaries in this project
  + Seems that probider might be somehow separated from other data
- Add biz/tech ids
  + There are currently no ids in project
  + Update/delete of single object now would require to match whole row
- Add versions to each row
  + Add row lock for update (if necessary in future)
- Add webworkers/caching for Frontend
- Add caching for backend
  + If data are non mutable then we can use some hash function for calculating is response same
